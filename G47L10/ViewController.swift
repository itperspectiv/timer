//
//  ViewController.swift
//  G47L10
//
//  Created by Ivan Vasilevich on 10/26/16.
//  Copyright © 2016 Ivan Besarab. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var interval = 0.1

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
     setupTimer()
        //jfkldjfhdshsfkljfklsk
         Timer.scheduledTimer(timeInterval: interval, target: self, selector: #selector(foo), userInfo: nil, repeats: true)
        //add new timer
    }
    
    func foo(ti: Timer) {
        print("ti")
        let rand = arc4random()%100
        print("rand = \(rand)")
        if rand == 68 {
            ti.invalidate()
        }
    }
    
    func setupTimer() {
        Timer.scheduledTimer(timeInterval: interval, target: self, selector: #selector(tick), userInfo: nil, repeats: false)
    }

    func tick() {
        view.backgroundColor = UIColor(red: CGFloat(arc4random_uniform(255))/255, green: CGFloat(arc4random_uniform(255))/255, blue: CGFloat(arc4random_uniform(255))/255, alpha: 1)
        interval += 0.01
        setupTimer()
    }

    @IBAction func panRecognized(_ sender: UIPanGestureRecognizer) {
        print(sender.location(in: view))
    }
}

